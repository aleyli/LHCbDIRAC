.. _installvobox:

=================
Setup a new vobox
=================

In order to install a vobox you need a configuration file so called install.cfg.  If the machine correctly created, this configuration file
will be in /home/dirac directory. 

Please follow `this instructions <https://dirac.readthedocs.io/en/latest/AdministratorGuide/InstallingDIRACService/index.html#additional-server-installation>`_
and use the configuration file from /home/dirac/install.cfg

Make sure the dirac.cfg file is correctly created in the machine.