.. _administrator_guide:

===================
Administrator Guide
===================


This page is the work in progress. See more material here soon !

.. toctree::
   :maxdepth: 1

   Installation/make_release.rst
   Installation/certificate.rst
   pitExport.rst
   onlineDIRAC.rst
   Bookkeeping/index.rst
   dataDistribution.rst
   ReStripping.rst
   flushing.rst
   BOINC/index.rst
   Recipes/index.rst
   logging.rst
   lhcbMessageQueues
   sandboxStore
   Popularity/index.rst
   LHCbWebApp/index.rst
   Elasticsearch/esdocs.rst
   install
   CommandReference/Admin/index.rst
   Configuration/ExampleConfig.rst
