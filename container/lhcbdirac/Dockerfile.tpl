# This is a templated Dockerfile. The final Dockerfile is generated by
# by just replacing $$LHCB_DIRAC_VERSION$$ with the correct value
# Dirac-docker-mgmt can do it, or gitlab-ci

FROM cern/cc7-base
MAINTAINER Christophe HAEN <christophe.haen@cern.ch>

RUN mkdir -p /opt/dirac/etc

# Copy the self pinging script
COPY dirac_self_ping.py /opt/dirac/

WORKDIR /opt/dirac

COPY dockerEntrypoint.sh /opt/dirac/dockerEntrypoint.sh
RUN chmod 755 /opt/dirac/dockerEntrypoint.sh
ENTRYPOINT [ "/opt/dirac/dockerEntrypoint.sh" ]

# Specify the version
ENV LHCB_DIRAC_VERSION $$LHCB_DIRAC_VERSION$$

RUN curl -L -o dirac-install https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/Core/scripts/dirac-install.py && chmod +x dirac-install && ./dirac-install -r $LHCB_DIRAC_VERSION -l LHCb -e LHCb -t server -i 27 && rm -rf /opt/dirac/.installCache

# Copy the script so that when logging interactively the environment is correct
RUN cp /opt/dirac/bashrc /root/.bashrc
