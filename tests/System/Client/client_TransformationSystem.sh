#!/bin/bash
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#
echo "lhcb-proxy-init -g lhcb_prmgr"
lhcb-proxy-init -g lhcb_prmgr
if [ $? -ne 0 ]
then
   exit $?
fi
echo " "


#Values to be used
userdir=$( echo "$USER" |cut -c 1)/$USER
stamptime=$(date +%Y%m%d_%H%M%S)
stime=$(date +"%H%M%S")
tdate=$(date +"20%y-%m-%d")
ttime=$(date +"%R")
version=${dirac-version}
mkdir -p TransformationSystemTest
directory=/lhcb/certification/Test/INIT/$version/$tdate/$stime
#selecting a random USER Storage Element
#SEs=$(dirac-dms-show-se-status |grep USER |grep -v 'Banned\|Degraded\|-2' | awk '{print $1}')

SEs=$(dirac-dms-show-se-status |grep BUFFER |grep -v 'Banned\|Degraded\|-new' | awk '{print $1}')

x=0
for n in $SEs
do
  arrSE[x]=$n
  let x++
done
# random=$[ $RANDOM % $x ]
# randomSE=${arrSE[$random]}

echo ""
echo "Submitting test production"
python $DIRAC/LHCbDIRAC/tests/System/Client/dirac-test-production.py -ddd
if [ $? -ne 0 ]
then
   exit $?
fi

transID=`cat TransformationID`

# Create unique files and adding entry to the bkk"
echo ""
echo "Creating unique test files and adding entry to the bkk"
./client_Bookkeeping.sh --Files=5 --Name="Test_Transformation_System_" --Path=$PWD/TransformationSystemTest/

# Add the random files to the transformation
echo ""
echo "Adding files to Storage Element $randomSE"
# filesToUpload=$(ls TransformationSystemTest/)
# for file in $filesToUpload
# do
#   random=$[ $RANDOM % $x ]
#   randomSE=${arrSE[$random]}
#   echo "$directory/$file \
#        ./TransformationSystemTest/$file $randomSE" \
#        >> TransformationSystemTest/LFNlist.txt
# done

while IFS= read -r line
do
  random=$[ $RANDOM % $x ]
  randomSE=${arrSE[$random]}
  echo "$line $randomSE"
done < LFNlist.txt >> ./LFNlistNew.txt

dirac-dms-add-file LFNlistNew.txt

LFNlist=$(cat LFNlist.txt | awk -vORS=, '{print $1}')

echo ""
echo "Adding the files to the test production:" $transID 
dirac-transformation-add-files $transID --LFNs $LFNlist

if [ $? -ne 0 ]
then
  exit $?
fi

# TODO: ___ Use Ramdom SEs___