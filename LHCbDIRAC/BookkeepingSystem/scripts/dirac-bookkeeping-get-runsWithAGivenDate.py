#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
# File :    dirac-bookkeeping-get-runsWithAGivenDate.py
# Author :  Zoltan Mathe
########################################################################
"""Retrieve from the Bookkeeping runs from a given date range."""
__RCSID__ = "$Id$"

from DIRAC.Core.Base import Script
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

Script.setUsageMessage(__doc__ + '\n'.join([
    'Usage:',
    '  %s [option|cfgfile] ... Start [End]' % Script.scriptName,
    'Arguments:',
    '  Start:    Start date (Format: YYYY-MM-DD)',
    '  End:      End date (Format: YYYY-MM-DD). Default is Start']))
Script.parseCommandLine(ignoreErrors=True)
args = Script.getPositionalArgs()

start = ''
end = ''
if len(args) > 2 or not args or not args[0]:
  Script.showHelp()

if len(args) == 2:
  end = args[1]
start = args[0]

in_dict = {}
in_dict['StartDate'] = start
in_dict['EndDate'] = end if end else start

res = BookkeepingClient().getRunsForAGivenPeriod(in_dict)
if not res['OK']:
  print 'ERROR: Failed to retrieve runs: %s' % res['Message']
else:
  if not res['Value']['Runs']:
    print 'No runs found for the date range', start, end
  else:
    print 'Runs:', res['Value']['Runs']
    if 'ProcessedRuns' in res['Value']:
      print 'Processed runs:', res['Value']['ProcessedRuns']
    if 'NotProcessedRuns' in res['Value']:
      print 'Not processed runs:', res['Value']['NotProcessedRuns']
